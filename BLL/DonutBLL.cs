﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using Models;
using System.ComponentModel;
using System.Media;

namespace BLL
{
    public class DonutBLL
    {
        private static Student student;

        public static void InitStudent(int StudentId)
        {
            if(student == null || StudentId != student.Id)
                student = StudentDAL.Find(StudentId);
        }

        public static void EatDonut(int StudentId)
        {
            BiteDonut(StudentId, 3);
        }

        public static void DonutOverload(Student student, byte bite)
        {
            byte overload = (byte)(bite - student.Donut);
            byte tickets = (byte)(1 + overload / Student.defaultDonnut);
            SoundPlayer sound = new SoundPlayer("Sounds/TimHortons.wav");
            sound.Play();
            student.TimHortonTickets += tickets;
            student.Donut = (byte)(Student.defaultDonnut - overload);
        }

        public static void BiteDonut(int StudentId, byte bite = 1)
        {
            InitStudent(StudentId);
            if (bite >= student.Donut)
                DonutOverload(student, bite);
            else
                student.Donut -= bite;
            StudentDAL.ApplyChange(student);
        }

        public static void RemoveTimHortonTickets(int StudentId)
        {
            InitStudent(StudentId);
            if(student.TimHortonTickets > 0) {
                student.TimHortonTickets--;
                StudentDAL.ApplyChange(student);
            }
        }
    }
}
