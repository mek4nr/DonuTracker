﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using System.Diagnostics;

namespace BLL
{
    public class GroupBLL
    {
        public static Group ActiveGroup { get; set; }

        public static Group Add(String name)
        {
            Group group = null;

            if (GroupDAL.Filter(g => g.Name == name && g.UserId == UserBLL.ActiveUser.Id).Count == 0)
            {
                group = new Group { Name = name, UserId = UserBLL.ActiveUser.Id };
                GroupDAL.Add(group);
                StudentDAL.Add(new Student() { FirstName = UserBLL.ActiveUser.Pseudo, LastName = "Profesor", UserId = UserBLL.ActiveUser.Id, GroupId = group.Id });
            }
            return group;
        }

        public static bool Update(String name, Group group)
        {
            if (GroupDAL.Filter(g => g.Name == name && g.UserId == UserBLL.ActiveUser.Id).Count == 0)
            {
                group.Name = name;
                GroupDAL.ApplyChange(group);
                return true;
            }
            return false;
        }

        public static void SelectGroup(Group group)
        {
            if (ActiveGroup == null || ActiveGroup.UserId != group.UserId)
                ActiveGroup = GroupDAL.Find(group.Id);
        }

        public static bool Remove(int groupId)
        {
            if (GroupDAL.Find(groupId) != null)
            {
                GroupDAL.Remove(groupId);
                return true;
            }
            return false;
        }

        public static List<Group> AllGroupOfActiveUser()
        {
            List<Group> listGroup = GroupDAL.Filter(g => g.UserId == UserBLL.ActiveUser.Id);
            
            // update NumberofStudents + CriticalStudents
            foreach (Group g in listGroup)
            {
                g.NumberOfStudents = StudentDAL.Filter(s => s.GroupId == g.Id).Count;
                g.CriticalStudents = StudentDAL.Filter(s => s.GroupId == g.Id && s.Donut < 5).Count;
            }

            return listGroup;
        }
    }
}
