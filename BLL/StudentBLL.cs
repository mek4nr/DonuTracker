﻿using DAL;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class StudentBLL
    {
        public static Student Add(string firstName, string lastName)
        {
            Student stu = null;

            if (StudentDAL.Filter(s => s.FirstName == firstName && s.LastName == lastName && s.GroupId == GroupBLL.ActiveGroup.Id).Count == 0)
            {
                stu = new Student() { FirstName = firstName, LastName = lastName, GroupId = GroupBLL.ActiveGroup.Id };
                StudentDAL.Add(stu);
            }
            return stu;
        }

        public static List<Student> FindAllStudentsOfActiveGroup()
        {
            return StudentDAL.Filter(s => s.GroupId == GroupBLL.ActiveGroup.Id);
        }

        public static bool Update(string firstName, string lastName, Student stu)
        {
            if (StudentDAL.Filter(s => s.FirstName == firstName && s.LastName == lastName && s.GroupId == GroupBLL.ActiveGroup.Id).Count == 0)
            {
                stu.FirstName = firstName;
                stu.LastName = lastName;
                StudentDAL.ApplyChange(stu);
                return true;
            }
            return false;
        }

        public static bool Remove(int stuId)
        {
            Student student = StudentDAL.Find(stuId);
            if (student != null && student.UserId == null)
            {
                StudentDAL.Remove(stuId);
                return true;
            }
            return false;
        }
    }
}
