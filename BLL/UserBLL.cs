﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using DAL;

namespace BLL
{
    public static class UserBLL
    {
        public static User ActiveUser { get; set; }

        private static string[] ForbiddenPseudo = { "test", "admin", "couillon" };

        public static bool Login(string login, string password)
        {
            User user = new User() { Pseudo = login, Password = password };
            List<User> users = UserDAL.Filter(u => u.Pseudo == user.Pseudo && u.Password == user.Password);
            if (users.Count == 1)
            {
                ActiveUser = users.First();
                return true;
            }
            return false;
        }

        public static bool Logout()
        {
            if(ActiveUser != null)
            {
                ActiveUser = null;
                return true;
            }
            return false;
        }

        public static bool Subscribe(string login, string password, bool debug = false)
        {
            User user = new User() { Pseudo = login, Password = password };
            List<User> users = UserDAL.Filter(u => u.Pseudo == user.Pseudo);

            bool forbidden = false;

            if (!debug)
            {
                foreach (string pseudo in ForbiddenPseudo)
                {
                    if (login == pseudo)
                    {
                        forbidden = true;
                        break;
                    }
                }
            }

            if (!forbidden && users.Count == 0)
            {
                UserDAL.Add(user);
                ActiveUser = user;
                return true;
            }
            return false;
        }

        public static bool IsLogged()
        {
            return ActiveUser != null;
        }

    }
}
