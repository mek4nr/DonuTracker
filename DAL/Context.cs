﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Models;
using System.Data.Entity.ModelConfiguration;

namespace DAL
{
    public class Context :  DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Student> Students { get; set; }
        public object ObjectStateManager { get; internal set; }

        public Context() : base("DonuTracker")
        {
        }
    }

}
