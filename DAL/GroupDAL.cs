﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using System.Linq.Expressions;
using System.Diagnostics;

namespace DAL
{
    public class GroupDAL
    {

        public static void ApplyChange(Group group)
        {
            using (var db = new Context())
            {
                db.Entry(group).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
        }

        public static int? Add(Group group)
        {
            int? id;
            using (var db = new Context())
            {
                db.Groups.Add(group);
                db.SaveChanges();
                id = group.Id;
            }
            return id;
        }


        public static List<Group> All()
        {
            List<Group> groups = new List<Group>();

            using (var db = new Context())
            {
                var query = from b in db.Groups
                            orderby b.UserId
                            select b;

                foreach (Group group in query)
                {
                    groups.Add(group);
                }
            }

            return groups;
        }

        public static Group Find(params object[] keyvalue)
        {
            Group group = null;
            using (var db = new Context())
            {
                group = db.Groups.Find(keyvalue);
            }
            return group;
        }

        public static List<Group> Filter(Expression<Func<Group, bool>> predicate)
        {
            List<Group> groups = new List<Group>();
            using (var db = new Context())
            {
                foreach (Group u in db.Groups.Where(predicate))
                {
                    groups.Add(u);
                }
            }
            return groups;
        }

        public static void Remove(params object[] keyvalue)
        {
            using (var db = new Context())
            {
                Group g = db.Groups.Find(keyvalue);

                foreach(Student stu in StudentDAL.Filter(s => s.GroupId == g.Id))
                {
                    db.Students.Attach(stu);
                    db.Students.Remove(stu);
                }
                db.SaveChanges();
                db.Groups.Remove(g);
                db.SaveChanges();
            }
        }
    }
}
