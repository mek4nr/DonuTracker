﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Data;

namespace DAL
{
    public static class StudentDAL
    {
        public static void ApplyChange(Student student)
        {
            using (var db = new Context())
            {
                db.Entry(student).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
        }

        public static int? Add(Student student)
        {
            int? id;
            using (var db = new Context())
            {
                db.Students.Add(student);
                db.SaveChanges();
                id = student.Id;
            }
            return id;
        }

        public static List<Student> All()
        {
            List<Student> students = new List<Student>();

            using (var db = new Context())
            {
                var query = from b in db.Students
                            orderby b.Id
                            select b;
                foreach (Student student in query)
                {
                    students.Add(student);
                }
            }

            return students;
        }

        public static Student Find(params object[] keyvalue)
        {
            Student student = null;
            using (var db = new Context())
            {
                student = db.Students.Find(keyvalue);
            }
            return student;
        }

        public static List<Student> Filter(Expression<Func<Student, bool>> predicate)
        {
            List<Student> students = new List<Student>();
            using (var db = new Context())
            {
                foreach (Student u in db.Students.Where(predicate))
                {
                    students.Add(u);
                }
            }
            return students;
        }

        public static void Remove(params object[] keyvalue)
        {
            using (var db = new Context())
            {
                db.Students.Remove(db.Students.Find(keyvalue));
                db.SaveChanges();
            }
        }
    }

}
