﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using System.Linq.Expressions;

namespace DAL
{
    public class UserDAL
    {
        public static int? Add(User user)
        {
            int? Id;
            using (var db = new Context())
            {
                db.Users.Add(user);
                Id = user.Id;
                db.SaveChanges();
            }
            return Id;
        }

        public static List<User> All()
        {
            List<User> users = new List<User>();

            using (var db = new Context())
            {
                var query = from b in db.Users
                            orderby b.Id
                            select b;
                foreach (User user in query)
                {
                    users.Add(user);
                }
            }

            return users;
        }

        public static User Find(params object[] keyvalue)
        {
            User user = null;
            using(var db = new Context())
            {
                user = db.Users.Find(keyvalue);
            }
            return user;
        }

        public static List<User> Filter(Expression<Func<User, bool>> predicate)
        {
            List<User> users = new List<User>();
            using (var db = new Context())
            {
                foreach (User u in db.Users.Where(predicate))
                {
                    users.Add(u);
                }
            }
            return users;
        }

        public static void Remove(params object[] keyvalue)
        {
            using (var db = new Context())
            {
                db.Users.Remove(db.Users.Find(keyvalue));
                db.SaveChanges();
            }
        }
    }
}
