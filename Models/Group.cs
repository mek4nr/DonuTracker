﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Models
{
    public class Group
    {
        [NotMapped]
        public int NumberOfStudents { get; set; }
        private int criticalStudents;

        [NotMapped]
        public int CriticalStudents
        {
            get
            {
                return criticalStudents;
            }
            set
            {

                criticalStudents = value;
                if (NumberOfStudents != 0)
                {
                    RatioCriticalStudent = (short)(100 * criticalStudents / NumberOfStudents);
                }
            }
        }

        [NotMapped]
        public short RatioCriticalStudent { get; set; }

        [Required]
        public int Id { get; set; }

        public int UserId { get; set; }
        public virtual User User { get; set; }

        public string Name { get; set; }
    }
}
