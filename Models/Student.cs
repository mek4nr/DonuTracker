﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Models
{
    public class Student
    {
        
        [NotMapped]
        public static byte defaultDonnut = 9;
        [NotMapped]
        public string Donut1 { get; set;}
        [NotMapped]
        public string Donut2 { get; set; }
        [NotMapped]
        public string Donut3 { get; set; }

        public int Id { get; set; }

        [Required]
        public string FirstName { get; set; }

        public string LastName { get; set; }

        private byte donut;
        public byte Donut { get {
                return donut;
            } 
            set {
                donut = value;
                
                if (value == 9) { Donut1 = "Images/donut3.png"; Donut2 = "Images/donut3.png"; Donut3 = "Images/donut3.png"; }
                else if (value >= 6) { Donut1 = "Images/donut3.png"; Donut2 = "Images/donut3.png"; Donut3 =string.Format("Images/donut{0}.png", value % 3); }
                else if(value >= 3) { Donut1 = "Images/donut3.png"; Donut2 = string.Format("Images/donut{0}.png", value % 3); Donut3 ="Images/donut0.png"; }
                else if(value >= 0) { Donut1 = string.Format("Images/donut{0}.png", value % 3); Donut2 = "Images/donut0.png"; Donut3 = "Images/donut0.png"; }
                
            } }

        public byte TimHortonTickets { get; set; } 

        public int? UserId { get; set; }
        public virtual User User { get; set; }

        
        public virtual Group Group { get; set; }
        public int? GroupId { get; set; }

        public Student()
        {
            Donut = defaultDonnut;
            TimHortonTickets = 0;
        }
    }
}
