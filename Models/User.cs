﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Models
{
    public class User
    {
        public int Id { get; set; }
        public string Pseudo { get; set; }

        private string password;
        public string Password {
            get
            {
                return password;
            }
            set
            {
                password = Hash(value);
            }
        }

        private string Hash(string value)
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(value));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }

        public override string ToString()
        {
            return String.Format("ID:{0}, Login:{1}, Passwork:{2}", Id, Pseudo, Password);
        }
    }
}
