﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using BLL;
using DAL;
using System.Data.Entity.Infrastructure;

namespace TestProject
{
    public static class UserBLLUnitTest
    {
        public static void TestLogin()
        {
            Console.WriteLine("===== TEST LOGIN =====");
            User user = new User() { Pseudo = "test", Password = "test" };

            User userFail = new User() { Pseudo = user.Pseudo, Password = "" };

            UserDAL.Add(user);

            Dictionary<string, bool> testBool = new Dictionary<string, bool>();

            testBool.Add("Test fail login : ", !UserBLL.Login("test", ""));
            testBool.Add("Test is loged fail : ", !UserBLL.IsLogged());
            testBool.Add("Test logout fail : ", !UserBLL.Logout());
            testBool.Add("Test login : ", UserBLL.Login("test", "test"));
            testBool.Add("Test is loged : ", UserBLL.IsLogged());
            testBool.Add("Test logout : ", UserBLL.Logout());
            testBool.Add("Test is loged after logout : ", !UserBLL.IsLogged());

            foreach (KeyValuePair<string, bool> item in testBool)
            {
                Console.WriteLine(String.Format("{0}{1}", item.Key, (item.Value) ? "Passed" : "Failed"));
            }

            UserDAL.Remove(user.Id);
            Console.WriteLine("======================");
        }

        public static void TestSubscribe()
        {
            Console.WriteLine("===== TEST SUBSCRIBE =====");
            User user = new User() { Pseudo = "test", Password = "test" };

            Dictionary<string, bool> testBool = new Dictionary<string, bool>();

            testBool.Add("Test Subscribe : ", UserBLL.Subscribe(user.Pseudo, user.Password, true));
            testBool.Add("Test loged after subscribe : ", UserBLL.IsLogged());
            testBool.Add("Test fail subscribe : ", !UserBLL.Subscribe(user.Pseudo, "zqdz", true));

            foreach (KeyValuePair<string, bool> item in testBool)
            {
                Console.WriteLine(String.Format("{0}{1}", item.Key, (item.Value) ? "Passed" : "Failed"));
            }
            
            if(UserDAL.Filter(u => u.Pseudo == user.Pseudo).Count >= 1)
            {
                UserDAL.Remove(UserDAL.Filter(u => u.Pseudo == user.Pseudo).First().Id);
            }
            Console.WriteLine("==========================");
        }
    }

    public static class DonutBLLUnitTest
    {
        public static void TestBiteDonuts()
        {
            Console.WriteLine("===== TEST BITE =====");
            Student student = new Student() { FirstName = "test", LastName = "test" };
            StudentDAL.Add(student);

            Dictionary<string, bool> testBool = new Dictionary<string, bool>();

            testBool.Add("Test init : ", (student.Donut == 9 && student.TimHortonTickets == 0) ? true : false);
            DonutBLL.BiteDonut(student.Id);
            student = StudentDAL.Find(student.Id);
            testBool.Add("Test bite : ", (student.Donut == 8) ? true:false);
            testBool.Add("Tickets after bite : ", (student.TimHortonTickets == 0) ? true : false);
            DonutBLL.EatDonut(student.Id);
            student = StudentDAL.Find(student.Id);
            testBool.Add("Test eat 1 donut : ", (student.Donut == 5) ? true : false);
            testBool.Add("Tickets after eat 1 donut : ", (student.TimHortonTickets == 0) ? true : false);
            DonutBLL.BiteDonut(student.Id,5);
            student = StudentDAL.Find(student.Id);
            testBool.Add("Test eat all donuts : ", (student.Donut == 9) ? true : false);
            testBool.Add("Tickets apres all donuts  : ", (student.TimHortonTickets == 1) ? true : false);
            DonutBLL.BiteDonut(student.Id, 11);
            student = StudentDAL.Find(student.Id);
            testBool.Add("Test more than all donuts : ", (student.Donut == 7) ? true : false);
            testBool.Add("Tickets apres more than all donuts : ", (student.TimHortonTickets == 2) ? true : false);
            DonutBLL.RemoveTimHortonTickets(student.Id);
            student = StudentDAL.Find(student.Id);
            testBool.Add("Tickets after remove : ", (student.TimHortonTickets == 1) ? true : false);

            foreach (KeyValuePair<string, bool> item in testBool)
            {
                Console.WriteLine(String.Format("{0}{1}", item.Key, (item.Value) ? "Passed" : "Failed"));
            }

            StudentDAL.Remove(student.Id);
            Console.WriteLine("======================");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            UserBLLUnitTest.TestLogin();
            UserBLLUnitTest.TestSubscribe();
            DonutBLLUnitTest.TestBiteDonuts();
            Console.ReadKey();
        } 
    }
}
