﻿using System.Windows;
using BLL;
using Models;
using System.Media;
using System.IO;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public User User { get; set; }

        public MainWindow()
        {
            InitializeComponent();
        }

        private void GoGroupView()
        {
            this.Hide();
            WindowGroups windowGroups = new WindowGroups() { MainWindow = this };
            windowGroups.ShowDialog();
        }

        private void btnEntrer_Click(object sender, RoutedEventArgs e)
        {      
            // Validation compte+ mot de passe System.IO.File.WriteAllText(@"Sounds/Crunch.txt", "blabla");

            bool valid = UserBLL.Login(txbLogin.Text, txbPassword.Password);
            if (valid)
            {
                GoGroupView();
            }
            else
            {
                MessageBox.Show("Compte ou mot de passe invalide");
            }
        }

        private void btnCreer_Click(object sender, RoutedEventArgs e)
        {
            // Création compte+ mot de passe
            bool valid = UserBLL.Subscribe(txbLogin.Text, txbPassword.Password);
            if (valid)
            {
                GoGroupView();
            }
            else
            {
                MessageBox.Show("Pseudo déjà utilisé ou invalide");
            }
        }

        private void MenuItemExit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
