﻿using BLL;
using Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfApp1;

namespace Views
{
    /// <summary>
    /// Interaction logic for WindowAddStudent.xaml
    /// </summary>
    public partial class WindowAddStudent : Window
    {
        public WindowStudents WindowStudents { get; set; }
        public bool modifyState { get; set; }
        public Student stu { get; set; }

        public WindowAddStudent()
        {
            InitializeComponent();
        }

        private void btnInsert_Click(object sender, RoutedEventArgs e)
        {
            if (txtFirstName.Text != string.Empty && txtlLastName.Text != string.Empty)
            {
                StudentBLL.Add(txtFirstName.Text, txtlLastName.Text);
                this.Close();
            }
            else
            {
                MessageBox.Show("Veillez a bien inserer du text et a inserer un chiffre entre 0 et 9 pour les donuts");
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (modifyState)
            {
                btnInsert.Content = "Modifier etudiant";
                txtFirstName.Text = stu.FirstName;
                txtlLastName.Text = stu.LastName;
                btnInsert.IsEnabled = false;
                btnInsert.Visibility = Visibility.Hidden;
                btnModify.IsEnabled = true;
                btnModify.Visibility = Visibility.Visible;
            }
        }

        private void btnModify_Click(object sender, RoutedEventArgs e)
        {
            if (txtFirstName.Text != string.Empty && txtlLastName.Text != string.Empty)
            {
                StudentBLL.Update(txtFirstName.Text, txtlLastName.Text, stu);
                this.Close();
            }
            else
            {
                MessageBox.Show("Veillez a bien inserer du text et a inserer un chiffre entre 0 et 9 pour les donuts");
            }
        }
    }
}
