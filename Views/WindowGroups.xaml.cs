﻿using BLL;
using Models;
using System.Collections.Generic;
using System.Diagnostics;
using System.Timers;
using System.Windows;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for WindowGroupes.xaml
    /// </summary>
    public partial class WindowGroups : Window
    {
        public MainWindow MainWindow { get; set; }
        public Group Group { get; set; }

        public WindowGroups()
        {
            InitializeComponent();
        }

        private void GoMainWindow()
        {
            this.Hide();
            MainWindow.ShowDialog();
        }

        private void GoStudentsWindow()
        {
            WindowStudents windowStudents = new WindowStudents() { WindowGroups = this };
            this.Hide();
            windowStudents.ShowDialog();
        }

        
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
           
            Debug.WriteLine("Window Loaded");
            LBGroups.ItemsSource = GroupBLL.AllGroupOfActiveUser();
        }

        private void MenuItemLogOut_Click(object sender, RoutedEventArgs e)
        {
            UserBLL.Logout();
            GoMainWindow();
        }

        private void MenuItemExit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnAddGroup_Click(object sender, RoutedEventArgs e)
        {
            if(TBGroupName.Text != string.Empty)
            {
                if (GroupBLL.Add(TBGroupName.Text) != null)
                    LBGroups.ItemsSource = GroupBLL.AllGroupOfActiveUser();
                else
                    MessageBox.Show("Le groupe existe déjà!");
            }
            else
            {
                MessageBox.Show("You need to give a name");
            }
            
        }

        private void BtnRemoveGroup_Click(object sender, RoutedEventArgs e)
        {
            Group group = (Group)LBGroups.SelectedItem;
            if(group != null)
            {
                MessageBoxResult res = MessageBox.Show("Etes-vous sûr de vouloir supprimer le groupe: " + group.Name, "Supression", MessageBoxButton.YesNo);
                if (res == MessageBoxResult.Yes)
                {
                    if (GroupBLL.Remove(group.Id))
                    {
                        LBGroups.ItemsSource = GroupBLL.AllGroupOfActiveUser();
                    }
                    else
                    {
                        MessageBox.Show("This group doesn't exist, or nothing is selected");
                    }
                }
            }
        }

        private void BtnSelectGroup_Click(object sender, RoutedEventArgs e)
        {
            if (LBGroups.SelectedItem != null)
            {
                Group group = (Group)LBGroups.SelectedItem;
                GroupBLL.SelectGroup(group);

                GoStudentsWindow();
            }
        }

        private void BtnUpdateGroup_Click(object sender, RoutedEventArgs e)
        {

            if (TBGroupName.Text != string.Empty && LBGroups.SelectedItem != null)
            {
                GroupBLL.Update(TBGroupName.Text, (Group)LBGroups.SelectedItem);
                LBGroups.ItemsSource = GroupBLL.AllGroupOfActiveUser();
            }
            else
            {
                MessageBox.Show("You need to give a name or select an item");
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            MainWindow.Close();
        }
    }
}
