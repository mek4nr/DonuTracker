﻿using Models;
using BLL;
using System.Collections.Generic;
using System.Windows;
using System;
using Views;
using System.Diagnostics;
using System.Media;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for WindowEtudiant.xaml
    /// </summary>
    public partial class WindowStudents : Window
    {
        public WindowGroups WindowGroups { get; set; }

        public WindowStudents()
        {
            InitializeComponent();
        }

        private void MenuItem_Logout_Click(object sender, RoutedEventArgs e)
        {
            UserBLL.Logout();
            this.Hide();
            WindowGroups.MainWindow.Show();
        }

        private void MenuItem_BackToGroups_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();

            // update Groups in WindowGroups
            WindowGroups.LBGroups.ItemsSource = null;
            WindowGroups.LBGroups.ItemsSource = GroupBLL.AllGroupOfActiveUser();

            WindowGroups.ShowDialog();
        }

        private void MenuItemExit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        
        private void BtnAddStudent_Click(object sender, RoutedEventArgs e)
        {
            WindowAddStudent wdwAddStudent = new WindowAddStudent { WindowStudents = this };
            wdwAddStudent.ShowDialog();
            UpdateDataGrid();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            UpdateDataGrid();
        }

        private void btnModifyStudent_Click(object sender, RoutedEventArgs e)
        {

            //Debug.WriteLine(  ((Student)datagridStudents.SelectedItem).LastName);
            WindowAddStudent wdwAddStudent = new WindowAddStudent { WindowStudents = this, stu = (Student)datagridStudents.SelectedItem, modifyState = true };
            wdwAddStudent.ShowDialog();

            UpdateDataGrid();
        }

        private void btnDeleteStudent_Click(object sender, RoutedEventArgs e)
        {
            StudentBLL.Remove(((Student)datagridStudents.SelectedItem).Id);

            UpdateDataGrid();
        }

        private void UpdateDataGrid()
        {
            datagridStudents.ItemsSource = null;
            datagridStudents.ItemsSource = StudentBLL.FindAllStudentsOfActiveGroup();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            WindowGroups.Close();           
        }

        private void btnRemoveTimHortons_Click(object sender, RoutedEventArgs e)
        {
            if ((Student)datagridStudents.SelectedItem != null)
            {
                Student stu = (Student)datagridStudents.SelectedItem;
                DonutBLL.RemoveTimHortonTickets(stu.Id);

                UpdateDataGrid();
            }

        }

        private void btnEatDonut_Click(object sender, RoutedEventArgs e)
        {
            if ((Student)datagridStudents.SelectedItem != null)
            {
                SoundPlayer sound = new SoundPlayer("Sounds/Crunch.wav");
                sound.Play();

                Student stu = (Student)datagridStudents.SelectedItem;
                DonutBLL.EatDonut(stu.Id);

                UpdateDataGrid();
            }
        }

        private void btnSmallBiteDonut_Click(object sender, RoutedEventArgs e)
        {
            if ((Student)datagridStudents.SelectedItem != null)
            {
                SoundPlayer sound = new SoundPlayer("Sounds/Crunch.wav");
                sound.Play();

                Student stu = (Student)datagridStudents.SelectedItem;
                DonutBLL.BiteDonut(stu.Id);

                UpdateDataGrid();
            }
        }

        private void datagridStudents_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            Student stu = (Student)datagridStudents.SelectedItem;
            btnDeleteStudent.IsEnabled = true;
            if (stu !=null && stu.UserId != null)
            {
                btnDeleteStudent.IsEnabled = false;
            }
        }
    }
}
